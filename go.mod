module elastic-demo-api

go 1.14

require (
	bitbucket.org/tunaiku/elasticsearch-helper v0.0.4
	github.com/elastic/go-elasticsearch/v7 v7.6.0
	github.com/go-chi/chi v4.1.1+incompatible
	github.com/go-chi/cors v1.1.1
	github.com/go-pg/migrations/v7 v7.1.10 // indirect
	github.com/go-pg/pg/v9 v9.1.6 // indirect
	github.com/joho/godotenv v1.3.0
	github.com/olivere/elastic/v7 v7.0.0
)
