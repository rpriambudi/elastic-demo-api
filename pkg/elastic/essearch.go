package esclient

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"log"
	"strings"

	"github.com/elastic/go-elasticsearch/v7"
	"github.com/elastic/go-elasticsearch/v7/esapi"

	esapihelper "bitbucket.org/tunaiku/elasticsearch-helper"
	"bitbucket.org/tunaiku/elasticsearch-helper/search"
)

type IESClientAdapter interface {
	Index(index string, body []byte, documentID string) error
	Delete(index string, documentID string) error
	Search(index string, query interface{}) interface{}
	GetDocumentById(index string, documentID string) interface{}
}

type ESClientAdapter struct {
	Client *elasticsearch.Client
}

func InitializeESClient(addresses string) (*ESClientAdapter, error) {
	client, err := elasticsearch.NewClient(elasticsearch.Config{
		Addresses: getElasticAddresses(addresses),
	})
	if err != nil {
		log.Printf("Error initializing client: %v \n", err)
		return nil, err
	}

	return &ESClientAdapter{
		Client: client,
	}, nil
}

func getElasticAddresses(addressraw string) []string {
	address := strings.Split(addressraw, ",")
	return address
}

func (ss ESClientAdapter) Index(index string, body []byte, documentID string) error {
	req := esapi.IndexRequest{
		Index:   index,
		Body:    strings.NewReader(string(body)),
		Refresh: "true",
	}

	if documentID != "" {
		req.DocumentID = documentID
	}
	_, err := req.Do(context.Background(), ss.Client)
	return err
}

func (ss ESClientAdapter) Delete(index string, documentID string) error {
	req := esapi.DeleteRequest{
		Index:      index,
		DocumentID: documentID,
		Refresh:    "true",
	}
	_, err := req.Do(context.Background(), ss.Client)
	return err
}

func (ss ESClientAdapter) Search(index string, query interface{}) interface{} {
	queryBuffer := query.(bytes.Buffer)
	res, err := ss.Client.Search(
		ss.Client.Search.WithContext(context.Background()),
		ss.Client.Search.WithIndex(index),
		ss.Client.Search.WithBody(&queryBuffer),
		ss.Client.Search.WithTrackTotalHits(true),
		ss.Client.Search.WithPretty(),
	)

	if err != nil {
		log.Printf("Elastic search err: %v \n", err)
		return nil
	}

	esresponse := esapihelper.WrapClientResponse(res)
	if esresponse.IsError() {
		log.Printf("Elasticsearch index err: %v \n", errors.New(esresponse.Status()))
		return nil
	}

	response, err := search.WrapSearchResponse(esresponse)
	if err != nil {
		log.Printf("Interal wrapper error: %v \n", err)
		return nil
	}

	return response
}

func (ss ESClientAdapter) GetDocumentById(index string, documentID string) interface{} {
	res, err := ss.Client.Get(index, documentID)
	if err != nil {
		log.Printf("Elastic search err: %v \n", err)
		return nil
	}

	if res.IsError() {
		log.Printf("Elasticsearch index err: %v \n", errors.New(res.Status()))
		return nil
	}

	defer res.Body.Close()
	var object map[string]interface{}

	if err := json.NewDecoder(res.Body).Decode(&object); err != nil {
		log.Printf("JSON decode err: %v \n", err)
		return nil
	}

	raw := object["_source"].(map[string]interface{})
	return raw
}
