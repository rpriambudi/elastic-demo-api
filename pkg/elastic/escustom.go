package esclient

import (
	"bytes"
	"context"
	"encoding/json"
	"log"

	"github.com/olivere/elastic/v7"
)

type ESCustomAdapter struct {
	Client *elastic.Client
}

func InitializeCustomESAdapter(addresses string) (*ESCustomAdapter, error) {
	client, err := elastic.NewClient(
		elastic.SetURL(addresses),
		elastic.SetSniff(false),
	)
	if err != nil {
		return nil, err
	}

	return &ESCustomAdapter{
		Client: client,
	}, nil
}

func (ec ESCustomAdapter) Index(index string, body []byte, documentID string) error {
	indexBuilder := ec.Client.Index().Index(index)
	if documentID != "" {
		indexBuilder.Id(documentID)
	}

	var bodyObj map[string]interface{}
	err := json.Unmarshal(body, &bodyObj)
	if err != nil {
		return err
	}

	_, err = indexBuilder.BodyJson(bodyObj).Do(context.Background())
	return err
}

func (ec ESCustomAdapter) Delete(index string, documentID string) error {
	_, err := ec.Client.Delete().Index(index).Id(documentID).Do(context.Background())
	return err
}

func (ec ESCustomAdapter) Search(index string, query interface{}) interface{} {
	queryBuffer := query.(bytes.Buffer)
	queryString := queryBuffer.String()

	response, err := ec.Client.Search().Index(index).Source(queryString).Do(context.Background())
	if err != nil {
		log.Printf("Query err: %v \n", err)
		return nil
	}

	return response
}

func (ec ESCustomAdapter) GetDocumentById(index string, documentID string) interface{} {
	result, err := ec.Client.Get().Index(index).Id(documentID).Do(context.Background())
	if err != nil {
		log.Printf("Query err: %v \n", err)
		return nil
	}

	var jsonObj map[string]interface{}
	json.Unmarshal(result.Source, &jsonObj)
	return jsonObj
}
