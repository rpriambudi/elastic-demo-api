package models

import (
	"time"
)

type Faq struct {
	tableName struct{} `pg:"mst_faqs"`

	Id        int       `json:"id" json:"id"`
	Question  string    `pg:"question" json:"question"`
	Answer    string    `pg:"answer" json:"answer"`
	QsType    string    `pg:"qs_type" json:"qsType"`
	CreatedAt time.Time `pg:"default:now()" json:"createdAt"`
}
