package models

import (
	"time"
)

type Category struct {
	Id        int       `json:"id"`
	Name      string    `pg:"name" json:"name"`
	CreatedAt time.Time `pg:"default:now()" json:"createdAt"`
	UpdatedAt time.Time `pg:"default:now()" json:"updatedAt"`
}
