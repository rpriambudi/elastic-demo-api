package models

type History struct {
	FaqId       int    `json:"faqId"`
	FaqQuestion string `json:"faqQuestion"`
}
