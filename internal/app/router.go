package app

import (
	"context"
	"net/http"

	"elastic-demo-api/internal/app/infrastructure"

	"github.com/go-chi/chi"
	"github.com/go-chi/cors"
)

// Route is a router for all FAQ related endpoints
func Route() *chi.Mux {
	faqcontroller := infrastructure.NewFaqController()
	chiRoute := chi.NewRouter()

	chiRoute.Use(cors.Handler(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Content-Type", "Authorization"},
		AllowCredentials: true,
	}))

	chiRoute.Route("/api/faqs", func(r chi.Router) {
		r.Get("/search", faqcontroller.SearchFaq)
		r.Get("/popular/keywords", faqcontroller.GetMostPopularQuery)
		r.Get("/popular/results", faqcontroller.GetMostPopularResult)

		r.Post("/index/all", faqcontroller.IndexAllData)

		r.Route("/{faqId}", func(r chi.Router) {
			r.Use(FaqContext)
			r.Get("/", faqcontroller.GetFaqByID)
		})
	})

	chiRoute.Route("/healthcheck", func(r chi.Router) {
		r.Get("/", faqcontroller.Healthcheck)
	})

	return chiRoute
}

// FaqContext is for processing faqId parameter in route
func FaqContext(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		faqID := chi.URLParam(r, "faqId")
		ctx := context.WithValue(r.Context(), "faqId", faqID)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

// for processing category param
func CategoryContext(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		categoryName := chi.URLParam(r, "categoryName")
		ctx := context.WithValue(r.Context(), "categoryName", categoryName)
		next.ServeHTTP(w, r.WithContext(ctx))
	})
}

func allowOriginFunc(r *http.Request, origin string) bool {
	return true
}
