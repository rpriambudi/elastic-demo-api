package search

import (
	"bytes"
	"elastic-demo-api/internal/app/master/models"
	esclient "elastic-demo-api/pkg/elastic"
	"encoding/json"
	"errors"
	"fmt"
	"os"

	"bitbucket.org/tunaiku/elasticsearch-helper/search"
	"github.com/olivere/elastic/v7"
)

type PopularSearch struct {
	Client esclient.IESClientAdapter
}

type PopularSearchResult struct {
	Documents []PopularDocument `json:"documents"`
}

type PopularDocument struct {
	DocCount    float64 `json:"docCount"`
	MostPopular string  `json:"mostPopular"`
}

func (ps PopularSearch) Index(i interface{}) (interface{}, error) {
	popular := i.(models.Popular)
	jsonObj, err := json.Marshal(popular)
	if err != nil {
		fmt.Printf("Data: %v", popular)
		fmt.Printf("Stacktrace: %v", err)
		return nil, err
	}

	err = ps.Client.Index(os.Getenv("ES_POPULAR_INDEX"), jsonObj, "")
	if err != nil {
		fmt.Printf("Data: %v", popular)
		fmt.Printf("Stacktrace: %v", err)
		return nil, err
	}

	return popular, nil
}

func (ps PopularSearch) Delete(id string) error {
	err := ps.Client.Delete(os.Getenv("ES_POPULAR_INDEX"), id)
	return err
}

func (ps PopularSearch) GetDocumentById(id string) (interface{}, error) {
	return nil, nil
}

func (ps PopularSearch) Search(query string) (interface{}, error) {
	queryObj, err := buildPopularQuery()
	if err != nil {
		return nil, err
	}

	response := ps.Client.Search(os.Getenv("ES_POPULAR_INDEX"), queryObj)
	if response == nil {
		return nil, nil
	}

	switch response.(type) {
	case search.SearchAPISuccessResponse:
		return parseESPopularResult(response.(search.SearchAPISuccessResponse)), nil
	case *elastic.SearchResult:
		return parseESCustomPopularResult(response.(*elastic.SearchResult)), nil
	default:
		return nil, errors.New("Type not recognized")
	}
}

func parseESPopularResult(raw search.SearchAPISuccessResponse) PopularSearchResult {
	var popular []PopularDocument
	for _, aggr := range raw.Aggregations["MostPopular"].Buckets {
		popular = append(popular, PopularDocument{DocCount: aggr["doc_count"].(float64), MostPopular: aggr["key"].(string)})
	}

	return PopularSearchResult{
		Documents: popular,
	}
}

func parseESCustomPopularResult(raw *elastic.SearchResult) PopularSearchResult {
	var popular []PopularDocument
	if agg, found := raw.Aggregations.Terms("MostPopular"); found {
		for _, bucket := range agg.Buckets {
			popular = append(popular, PopularDocument{DocCount: float64(bucket.DocCount), MostPopular: bucket.Key.(string)})
		}
	}

	return PopularSearchResult{
		Documents: popular,
	}
}

func buildPopularQuery() (bytes.Buffer, error) {
	queryObj := map[string]interface{}{
		"size": 0,
		"aggs": map[string]interface{}{
			"MostPopular": map[string]interface{}{
				"terms": map[string]interface{}{
					"field": "query.keyword",
					"size":  10,
				},
			},
		},
	}

	var queryBuffer bytes.Buffer
	err := json.NewEncoder(&queryBuffer).Encode(queryObj)

	return queryBuffer, err
}
