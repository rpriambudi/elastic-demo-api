package search

import (
	esclient "elastic-demo-api/pkg/elastic"
	"os"
	"strings"
)

// SearchProviders is the placeholder for search services
var SearchProviders map[string]BaseSearch = make(map[string]BaseSearch)

func Initialize() error {
	essearch, err := esclient.InitializeCustomESAdapter(os.Getenv("ES_HOST"))
	if err != nil {
		return err
	}

	faqsearch := FaqSearch{
		Client: essearch,
	}

	popularsearch := PopularSearch{
		Client: essearch,
	}

	historysearch := HistorySearch{
		Client: essearch,
	}

	SearchProviders["FaqSearch"] = faqsearch
	SearchProviders["PopularSearch"] = popularsearch
	SearchProviders["HistorySearch"] = historysearch
	return nil
}

func InitializeMock(mock esclient.IESClientAdapter) {
	faqsearch := FaqSearch{
		Client: mock,
	}
	SearchProviders["FaqSearch"] = faqsearch
}

type BaseSearch interface {
	Index(i interface{}) (interface{}, error)
	GetDocumentById(id string) (interface{}, error)
	Delete(id string) error
	Search(query string) (interface{}, error)
}

func getElasticAddresses() []string {
	addressraw := os.Getenv("ES_HOST")
	address := strings.Split(addressraw, ",")
	return address
}
