package search

import (
	"bytes"
	"elastic-demo-api/internal/app/master/models"
	esclient "elastic-demo-api/pkg/elastic"
	"encoding/json"
	"errors"
	"fmt"
	"os"

	"bitbucket.org/tunaiku/elasticsearch-helper/search"
	"github.com/olivere/elastic/v7"
)

type HistorySearch struct {
	Client esclient.IESClientAdapter
}

type HistorySearchResult struct {
	Documents []HistoryDocument `json:"documents"`
}

type HistoryDocument struct {
	DocCount    float64 `json:"docCount"`
	FaqId       float64 `json:"faqId"`
	FaqQuestion string  `json:"faqQuestion"`
}

const popularKeyword = "faqQuestion.raw"

func (hs HistorySearch) Index(i interface{}) (interface{}, error) {
	history := i.(models.History)
	jsonObj, err := json.Marshal(history)
	if err != nil {
		fmt.Printf("Data: %v", history)
		fmt.Printf("Stacktrace: %v", err)
		return nil, err
	}

	err = hs.Client.Index(os.Getenv("ES_HISTORY_INDEX"), jsonObj, "")
	if err != nil {
		fmt.Printf("Data: %v", history)
		fmt.Printf("Stacktrace: %v", err)
		return nil, err
	}

	return history, nil
}

func (hs HistorySearch) Delete(id string) error {
	err := hs.Client.Delete(os.Getenv("ES_HISTORY_INDEX"), id)
	return err
}

func (hs HistorySearch) GetDocumentById(id string) (interface{}, error) {
	return nil, nil
}

func (hs HistorySearch) Search(query string) (interface{}, error) {
	queryObj, err := buildHistoryQuery(query)
	if err != nil {
		return nil, err
	}

	response := hs.Client.Search(os.Getenv("ES_HISTORY_INDEX"), queryObj)
	if response == nil {
		return nil, nil
	}

	switch response.(type) {
	case search.SearchAPISuccessResponse:
		return parseESHistoryResult(response.(search.SearchAPISuccessResponse)), nil
	case *elastic.SearchResult:
		return parseESCustomHistoryResult(response.(*elastic.SearchResult)), nil
	default:
		return nil, errors.New("Type not recognized")
	}
}

func parseESHistoryResult(raw search.SearchAPISuccessResponse) HistorySearchResult {
	var history []HistoryDocument
	for _, aggr := range raw.Aggregations["PopularResultID"].Buckets {
		subAggr := aggr["PopularResultQuestion"].(map[string]interface{})
		subAggs := subAggr["buckets"].([]interface{})
		subKey := subAggs[0].(map[string]interface{})
		data := HistoryDocument{
			FaqId:       aggr["key"].(float64),
			DocCount:    aggr["doc_count"].(float64),
			FaqQuestion: subKey["key"].(string),
		}
		history = append(history, data)
	}

	return HistorySearchResult{
		Documents: history,
	}
}

func parseESCustomHistoryResult(raw *elastic.SearchResult) HistorySearchResult {
	var history []HistoryDocument
	if agg, found := raw.Aggregations.Terms("PopularResultID"); found {
		for _, bucket := range agg.Buckets {
			if subAgg, found := bucket.Terms("PopularResultQuestion"); found {
				data := HistoryDocument{
					FaqId:       bucket.Key.(float64),
					DocCount:    float64(bucket.DocCount),
					FaqQuestion: subAgg.Buckets[0].Key.(string),
				}
				history = append(history, data)
			}
		}
	}

	return HistorySearchResult{
		Documents: history,
	}
}

func buildHistoryQuery(query string) (bytes.Buffer, error) {
	queryObj := map[string]interface{}{
		"query": map[string]interface{}{
			"match": map[string]interface{}{
				"faqQuestion": query,
			},
		},
		"size": 0,
		"aggs": map[string]interface{}{
			"PopularResultID": map[string]interface{}{
				"terms": map[string]interface{}{
					"field": "faqId",
				},
				"aggs": map[string]interface{}{
					"PopularResultQuestion": map[string]interface{}{
						"terms": map[string]interface{}{
							"field": "faqQuestion.raw",
							"size":  5,
						},
					},
				},
			},
		},
	}

	var queryBuffer bytes.Buffer
	err := json.NewEncoder(&queryBuffer).Encode(queryObj)

	return queryBuffer, err
}
