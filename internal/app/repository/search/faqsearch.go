package search

import (
	"bytes"
	"elastic-demo-api/internal/app/master/models"
	esclient "elastic-demo-api/pkg/elastic"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"strconv"

	"bitbucket.org/tunaiku/elasticsearch-helper/search"
	"github.com/olivere/elastic/v7"
)

type FaqSearch struct {
	Client esclient.IESClientAdapter
}

type FaqSearchResult struct {
	Documents  []FaqDocument  `json:"documents"`
	Aggregates []FaqAggregate `json:"aggregates"`
}

type FaqDocument struct {
	Id       float64 `json:"id"`
	Question string  `json:"question"`
	Answer   string  `json:"answer"`
	QsType   string  `json:"qsType"`
}

type FaqAggregate struct {
	DocCount     float64 `json:"docCount"`
	CategoryName string  `json:"categoryName"`
}

func (fs FaqSearch) Index(i interface{}) (interface{}, error) {
	faq := i.(models.Faq)
	jsonObj, err := json.Marshal(faq)
	if err != nil {
		fmt.Printf("Data: %v \n", faq)
		fmt.Printf("Stacktrace: %v \n", err)
		return nil, err
	}

	err = fs.Client.Index(os.Getenv("ES_INDEX"), jsonObj, strconv.Itoa(faq.Id))
	if err != nil {
		fmt.Printf("Data: %v", faq)
		fmt.Printf("Stacktrace: %v", err)
		return nil, err
	}

	return faq, nil
}

func (fs FaqSearch) Delete(id string) error {
	err := fs.Client.Delete(os.Getenv("ES_INDEX"), id)
	return err
}

func (fs FaqSearch) GetDocumentById(id string) (interface{}, error) {
	result := fs.Client.GetDocumentById(os.Getenv("ES_INDEX"), id)
	if result == nil {
		return nil, nil
	}
	return result, nil
}

func (fs FaqSearch) Search(query string) (interface{}, error) {
	queryObj, err := buildQuery(query)
	if err != nil {
		return nil, err
	}

	response := fs.Client.Search(os.Getenv("ES_INDEX"), queryObj)
	if response == nil {
		return FaqSearchResult{}, nil
	}

	switch response.(type) {
	case search.SearchAPISuccessResponse:
		return parseESResult(response.(search.SearchAPISuccessResponse)), nil
	case *elastic.SearchResult:
		return parseESCustomResult(response.(*elastic.SearchResult)), nil
	default:
		return nil, errors.New("Type not recognized")
	}
}

func parseESResult(raw search.SearchAPISuccessResponse) FaqSearchResult {
	var faqdocs []FaqDocument
	var faqaggregates []FaqAggregate

	for _, r := range raw.Hits.Hits {
		var mapper *FaqDocument = &FaqDocument{}
		r.MapDocument(mapper)
		faqdocs = append(faqdocs, *mapper)
	}

	for _, aggr := range raw.Aggregations["CategoryAggs"].Buckets {
		faqaggregates = append(faqaggregates, FaqAggregate{DocCount: aggr["doc_count"].(float64), CategoryName: aggr["key"].(string)})
	}
	return FaqSearchResult{Documents: faqdocs, Aggregates: faqaggregates}
}

func parseESCustomResult(result *elastic.SearchResult) FaqSearchResult {
	var jsonObj FaqDocument
	var faqdocs []FaqDocument

	for _, r := range result.Hits.Hits {
		res, err := r.Source.MarshalJSON()
		if err != nil {
			fmt.Println("Error parsing: ", err)
			break
		}

		json.Unmarshal(res, &jsonObj)
		faqdocs = append(faqdocs, jsonObj)
	}

	return FaqSearchResult{Documents: faqdocs}
}

func buildQuery(query string) (bytes.Buffer, error) {
	queryObj := map[string]interface{}{
		"query": map[string]interface{}{
			"match_all": map[string]interface{}{},
		},
		"size": 80,
	}

	if query != "" {
		queryObj = map[string]interface{}{
			"query": map[string]interface{}{
				"multi_match": map[string]interface{}{
					"query":  query,
					"fields": []string{"question", "answer"},
				},
			},
			"size": 80,
		}
	}

	var queryBuffer bytes.Buffer
	err := json.NewEncoder(&queryBuffer).Encode(queryObj)

	return queryBuffer, err
}

func (fsr *FaqDocument) MapSearchResult(d search.Document) {
	fsr.Id = d.Source["id"].(float64)
	fsr.Question = d.Source["question"].(string)
	fsr.Answer = d.Source["answer"].(string)
	fsr.QsType = d.Source["qsType"].(string)
}
