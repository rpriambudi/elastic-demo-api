package app

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"elastic-demo-api/internal/app/repository/search"

	"github.com/joho/godotenv"
)

// Run is main function, which split commands
func Run() {
	err := godotenv.Load()
	if err != nil {
		exitf(err.Error())
	}

	RunServer()
}

// RunServer run the web server
func RunServer() {
	err := search.Initialize()
	if err != nil {
		exitf(err.Error())
	}

	log.Fatal(http.ListenAndServe(":1000", Route()))
}

func exitf(s string, args ...interface{}) {
	errorf(s, args...)
	os.Exit(1)
}

func errorf(s string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, s+"\n", args...)
}
