package infrastructure

import (
	"elastic-demo-api/internal/app/master/models"
	"elastic-demo-api/internal/app/service"
	"encoding/json"
	"net/http"
	"strconv"
)

// FaqController class which holds functions mapped to endpoints
type FaqController struct {
	appservice service.IAppService
}

type Healthcheck struct {
	status  float64
	message string
}

// NewFaqController create new instance of FaqController
func NewFaqController() FaqController {
	return FaqController{
		appservice: service.GetAppService(),
	}
}

// GetFaqByID ...
func (fc FaqController) GetFaqByID(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	faqID, ok := ctx.Value("faqId").(string)
	if !ok {
		http.Error(w, http.StatusText(422), 422)
		return
	}

	id, err := strconv.Atoi(faqID)
	if err != nil {
		http.Error(w, http.StatusText(422), 422)
		return
	}

	faq, err := fc.appservice.GetFaqById(id)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	if faq == nil {
		jsonObj, _ := json.Marshal(models.Faq{})
		w.Header().Set("Content-Type", "application/json")
		w.Write(jsonObj)
		return
	}

	jsonObj, err := json.Marshal(faq)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonObj)
}

// Search faq
func (fc FaqController) SearchFaq(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query().Get("query")
	results, err := fc.appservice.SearchFaq(query)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	jsonObj, err := json.Marshal(results)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonObj)
}

func (fc FaqController) GetMostPopularQuery(w http.ResponseWriter, r *http.Request) {
	results, err := fc.appservice.GetFaqMostPopularQuery()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	jsonObj, err := json.Marshal(results)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonObj)
}

func (fc FaqController) GetMostPopularResult(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query().Get("query")
	if query == "" {
		http.Error(w, "No query provided", 422)
		return
	}

	results, err := fc.appservice.GetFaqMostPopularResult(query)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	jsonObj, err := json.Marshal(results)
	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonObj)
}

func (fc FaqController) IndexAllData(w http.ResponseWriter, r *http.Request) {
	err := fc.appservice.IndexAllFaq()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Write([]byte("OK"))
}

// healthcheck
func (fc FaqController) Healthcheck(w http.ResponseWriter, r *http.Request) {
	health := Healthcheck{status: 200, message: "OK"}
	jsonObj, err := json.Marshal(health)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.Write(jsonObj)
}
