package service

import (
	"elastic-demo-api/internal/app/master/models"
	"elastic-demo-api/internal/app/repository/search"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"strconv"
	"time"
)

type IAppService interface {
	GetFaqById(id int) (interface{}, error)
	GetFaqMostPopularQuery() (interface{}, error)
	GetFaqMostPopularResult(query string) (interface{}, error)
	SearchFaq(query string) (interface{}, error)
	IndexAllFaq() error
}

type AppService struct {
	faqsearch     search.BaseSearch
	popularsearch search.BaseSearch
	historysearch search.BaseSearch
}

func GetAppService() IAppService {
	return AppService{
		faqsearch:     search.SearchProviders["FaqSearch"],
		popularsearch: search.SearchProviders["PopularSearch"],
		historysearch: search.SearchProviders["HistorySearch"],
	}
}

func (fs AppService) GetFaqById(id int) (interface{}, error) {
	result, err := fs.faqsearch.GetDocumentById(strconv.Itoa(id))
	if err != nil {
		return nil, err
	}

	raw := result.(map[string]interface{})
	faq := models.Faq{
		Id:       int(raw["id"].(float64)),
		Question: raw["question"].(string),
		Answer:   raw["answer"].(string),
		QsType:   raw["qsType"].(string),
	}
	go fs.historysearch.Index(models.History{
		FaqId:       faq.Id,
		FaqQuestion: faq.Question,
	})
	return faq, nil
}

func (fs AppService) SearchFaq(query string) (interface{}, error) {
	results, err := fs.faqsearch.Search(query)
	if err != nil {
		return nil, err
	}

	faqs := results.(search.FaqSearchResult)
	if query != "" && faqs.Documents != nil {
		go fs.popularsearch.Index(models.Popular{
			Query: query,
		})
	}

	return faqs, nil
}

func (fs AppService) GetFaqMostPopularQuery() (interface{}, error) {
	results, err := fs.popularsearch.Search("")
	if err != nil {
		return nil, err
	}

	return results, nil
}

func (fs AppService) GetFaqMostPopularResult(query string) (interface{}, error) {
	results, err := fs.historysearch.Search(query)
	if err != nil {
		return nil, err
	}

	return results, nil
}

func (fs AppService) IndexAllFaq() error {
	_, b, _, _ := runtime.Caller(0)
	basepath := filepath.Dir(b)
	filepath := basepath + "\\..\\..\\..\\sample.json"

	jsonFile, err := os.Open(filepath)
	if err != nil {
		fmt.Println("JSON err: ", err)
		return err
	}

	defer jsonFile.Close()

	var faqs []models.Faq
	byteData, _ := ioutil.ReadAll(jsonFile)

	json.Unmarshal(byteData, &faqs)

	startTime := time.Now()
	log.Printf("start indexing: %v \n", startTime)
	for _, faq := range faqs {
		_, err := fs.faqsearch.Index(faq)
		if err != nil {
			return err
		}
	}

	endTime := time.Now()
	log.Printf("finish indexing: %v \n", endTime)
	log.Printf("Time elapsed: %v \n", endTime.Sub(startTime).Milliseconds())
	return nil
}
