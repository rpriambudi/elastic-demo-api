FROM golang:1.14.4

WORKDIR $GOPATH/src/elastic-demo-api

COPY . .

RUN go install -v ./cmd/elastic-demo-api

EXPOSE 1000

CMD ["elastic-demo-api", "run"]
