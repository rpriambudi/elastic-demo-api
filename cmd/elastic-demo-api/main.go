package main

import app "elastic-demo-api/internal/app"

func main() {
	app.Run()
}
